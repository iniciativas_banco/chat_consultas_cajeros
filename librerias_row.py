from collections import namedtuple
import numpy as np
import folium
def getArrows(locations, color='blue', size=6, n_arrows=3):
    

    
    Point = namedtuple('Point', field_names=['lat', 'lon'])
    
   
    point1 = Point(locations[0][0], locations[0][1])
    point2 = Point(locations[1][0], locations[1][1])
    
    
    angle = get_angle(point1, point2) - 90
    
   

    arrow_latitude = np.linspace(point1.lat, point2.lat, n_arrows + 2)[1:n_arrows+1]
    arrow_longitude = np.linspace(point1.lon, point2.lon, n_arrows + 2)[1:n_arrows+1]
    
    final_arrows = []
    
    
    for points in zip(arrow_latitude, arrow_longitude):
        final_arrows.append(folium.RegularPolygonMarker(location=points, 
                      fill_color=color, number_of_sides=3, 
                      radius=size, rotation=angle))
    return final_arrows


def get_angle(p1, p2):
    
 
    
    longitude_diff = np.radians(p2.lon - p1.lon)
    
    latitude1 = np.radians(p1.lat)
    latitude2 = np.radians(p2.lat)
    
    x_vector = np.sin(longitude_diff) * np.cos(latitude2)
    y_vector = (np.cos(latitude1) * np.sin(latitude2) 
        - (np.sin(latitude1) * np.cos(latitude2) 
        * np.cos(longitude_diff)))
    angle = np.degrees(np.arctan2(x_vector, y_vector))
    
    # Checking and adjustring angle value on the scale of 360
    if angle < 0:
        return angle + 360
    return angle